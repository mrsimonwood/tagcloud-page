# README #

### What is this repository for? ###

* Wordpress Plugin to simply display a clean tagcloud page 
* Version 0.2.1

### How do I get set up? ###

* Depends on the DTC Shibboleth plugin.
* Upload to wp-content/plugins via FTP or Plugins > Add New > Upload Plugin
* In the dashboard choose "Activate" or "Activate Plugin"