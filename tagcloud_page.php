<?php
/**
 * @package Tagcloud_Page
 * @version 0.1
 */
/*
Plugin Name: Tagcloud Page
Bitbucket Plugin URI: https://bitbucket.org/mrsimonwood/tagcloud-page
Description: Simply display a clean tagcloud page
Version: 0.2.1
Author URI: http://simonwood.info/
*/

function tagcloud_page() {
	if (array_pop(explode('/', $_SERVER['REQUEST_URI'])) == "tagcloud-page") {
		wp_tag_cloud();
		die();
	}
}
add_action('template_redirect', 'tagcloud_page');

function target_tag_cloud_links( $return ) {
	$return = str_replace('">', '" target="_top">', $return );
	return $return;
}
add_filter( 'wp_tag_cloud', 'target_tag_cloud_links' );

function categorylist_page() {
	if (array_pop(explode('/', $_SERVER['REQUEST_URI'])) == "category-list-page") {
		wp_list_categories(array('show_count' => 1, 'title_li' => ''));
		die();
	}
}
add_action('template_redirect', 'categorylist_page');



?>
